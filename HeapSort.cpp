#include <fstream>
#include <iostream>
 
using namespace std;
 
void swap(int* a, int* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
 
void heap(int arr[], int n, int i)
{
    int max = i;   
    int l = 2*i + 1; 
    int r = 2*i + 2; 
    if (l < n && arr[l] > arr[max])
        max = l;
 
    if (r < n && arr[r] > arr[max])
        max = r;
    if (max != i)
    {
        swap(arr[i], arr[max]);
        heap(arr, n, max);
    }
}
 
void heapsort(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        heap(arr, n, i);
    for (int i = n-1; i >= 0; i--)
    {
        swap(arr[0], arr[i]);
        heap(arr, i, 0);
    }
}
 
int main()
{
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i <= n-1; i++)
    {
        cin >> arr[i];
    }
    heapsort(arr, n);
    for (int j = 0; j <= n-1; j++)
    {
        cout << arr[j];
        cout << " ";
    }
    return 0;
}