#include <fstream>
#include <iostream>

using namespace std; 

void Merg(long long arr[], long long begin, long long end, int n){
    long long i = begin, mid = begin + (end - begin) / 2, j = mid + 1, k = 0, d[n];
    while (i <= mid && j <= end){
        
        if (arr[i] <= arr[j]){
            
            d[k] = arr[i];
            i++;
            k++;
        }
        
        else{
            
            d[k] = arr[j];
            j++;
            k++;
        }
    }
    while (i <= mid){
        
        d[k] = arr[i];
        i++;
        k++;
    }
    while (j <= end){
        
        d[k] = arr[j];
        j++;
        k++;
    }
    for (i = 0; i < k; i++){
        
        arr[begin + i] = d[i];
    }
}
 
 
void MergSort(long long *arr, long long left, long long right, int n){
    
    long long temp = 0;
    if (left < right){
        
        if (right - left == 1){
            
            if (arr[left] > arr[right]){
                
                temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }
        }
        else{
            
            MergSort(arr, left, left + (right - left) / 2, n);
            MergSort(arr, left + (right - left) / 2 + 1, right, n);
            Merg(arr, left, right, n);
        }
    }
}

int main()
{
    
    int n, i = 0;
    cin >> n;
    long long arr[n];
    while (i <= n-1)
    {
        cin >> arr[i];
        i++;
    }
 
    MergSort(arr, 0, n - 1, n);
 
    for (int i = 0; i < n; i++){
        
        cout << arr[i] << ' ';
    }
    return 0;
}